using Roots, FastGaussQuadrature, Printf, FileIO, JLD2
using Interpolations, LinearAlgebra, SparseArrays, SuiteSparseGraphBLAS
include("MyCheb.jl")

"""
    scale_p(a,b,p)

Linear map of p ∈ [-1,1] to the interval [a,b].
"""
function scale_p(a::Float64,b::Float64,p::Float64)::Float64
    return (b-a)*p/2 + a/2+b/2
end

"""
    scale_w(a,b,w)

Linear map of the Gauss-Legendre points of [-1,1] to those of the interval [a,b].
"""
function scale_w(a::Float64,b::Float64,w::Float64)::Float64
    return (b-a)*w/2
end

"""
     IC_FP(params)

Computes the initial conditions of Pan & al 2021.
 params is a Dict with the following fields:
   - M          Mass of the central black hole
   - Ns 		Number of stars
   - ms			mass of each star
   - mbh		mass of each sBH
   - sigma		velocity dispersion (sets Dehnen scale radius to 4GM/sigma^2)
   - γ			inner slope of the Dehnen potential
   - M_u		mass unit (in S.I.)
   - v_u		velocity unit (in S.I.)
   - r_u		length unit (in S.I.)
   - t_fin		Length of the simulation in inner units
   - fin_cond   time or Pan&Yang for dNdt_bh|Egw = 0
   - NgridR		Grid size of the angular momentum variable R = J^2/J^2_c
   - Ngrids		Grid size of the energy variable R = J^2/J^2_c
   - N_coeffs	Number of points for Gauss quadratures
   - epsilon	Factor in the definition of dt. «f should change at top of epsilon its value»
   - N_steps	Number of big steps (recomputation of FP coefficients, re-evaluation of dt)
   - N_substeps Number of small steps (without recomputing the coefficients and dt)
   - N_save		Number of total substeps between snapshots
   - stars_lc   tde:       gives a different loss_cone for stars
   				[loss_cone]: the same
   - algorithm  [1]: flux conservative
   - scheme     [1]: explicit
   				2:  implicit
   - filename	base name of snapshot (e.g. "./snaps/out_")
   - final_output base name of final output (with rates & final DFs)

Otputs:
	- f0 		initial DF, NgridsxNgridRx2 array
	- aux 		auxiliary quantities, dictionary
"""
function IC_FP(params)
    #Extract the parameters of the problem
    delta  = params["delta"]
    M      = params["M"]
    Ns     = params["Ns"]
    Nbh    = delta*Ns
    γ      = params["γ"]

    if haskey(params, "γbh")
        γbh = params["γbh"]
    else
        γbh = params["γ"]
    end
    
    ms     = params["ms"]
    mbh    = params["mbh"]
    Ngrids = params["Ngrids"]
    NgridR = params["NgridR"]

    ## Dependent factors
    Ms    = ms * Ns
    Mbh   = mbh * Nbh
    sigma = params["sigma"]
    
    params["c"]   = 299792458.0 / params["v_u"]
    params["G"]   = 6.67428e-11 / (params["v_u"]^2 * params["r_u"] / params["M_u"])
    params["Gyr"] = 10^9 * (3600*24*365) / (params["r_u"]/params["v_u"])
    
    c     = params["c"]
    G     = params["G"]

    rh = G*M/sigma^2;
    ra = 4*rh;
    E0 = sigma^2/5
    A  = G*M;
    b1  = G*Ms;
    b2  = G*Mbh;
    Ai  = (3-γ)/4/pi * Ns* ra;
    Ai2  = (3-γbh)/4/pi * delta * Ns* ra;

    ##Analytic derivatives!
    
    phi(r)   = A/r - (b1*((r/(r + ra))^(2 - γ) - 1))/ra/(2 - γ) - (b2*((r/(r + ra))^(2 - γbh) - 1))/ra/(2 - γbh)
    phi_p(r) = - A/r^2 - b1*r^(1-γ)/(r+ra)^(3-γ)	- b2*r^(1-γbh)/(r+ra)^(3-γbh)		    # First derivative wrt r
    phi_s(r) = (2*A)/r^3 + (ra*γ + 2r - ra)*b1/r^γ/(r+ra)^(4-γ) + (ra*γ + 2r - ra)*b2/r^γbh/(r+ra)^(4-γbh)  # Second derivative wrt r

    r_p(r) = 1/phi_p.(r)										# First derivative wrt phi as a function of r
    r_s(r) = -phi_s(r)/phi_p(r)^3								# First derivative wrt phi as a function of r

    n(r)   = Ai/(r^γ*(r + ra)^(4-γ))							
    n_p(r) = -(4r+γ*ra)*Ai/(r+ra)^(5-γ)/r^(1+γ)					          # First derivative wrt r
    n_s(r) = Ai*(γ^2*ra^2 + 10γ*r*ra + γ*ra^2+20r^2)/(r+ra)^(6-γ)/r^(2+γ) # Second derivative wrt r
    
    nbh(r)   = Ai2/(r^γbh*(r + ra)^(4-γbh))							
    nbh_p(r) = -(4r+γbh*ra)*Ai2/(r+ra)^(5-γbh)/r^(1+γbh)					          # First derivative wrt r
    nbh_s(r) = Ai2*(γ^2*ra^2 + 10γbh*r*ra + γbh*ra^2+20r^2)/(r+ra)^(6-γbh)/r^(2+γbh)

    n_pp(r) = n_p(r)*r_p(r)										# First derivative wrt phi as a function of r
    n_ps(r) = n_s(r)*r_p(r)^2 + n_p(r)*r_s(r)					# Second derivative wrt phi as a function of r
    nbh_pp(r) = nbh_p(r)*r_p(r)										# First derivative wrt phi as a function of r
    nbh_ps(r) = nbh_s(r)*r_p(r)^2 + nbh_p(r)*r_s(r)					# Second derivative wrt phi as a function of r


 
 ## Loss cone radius:
    #it is the pericenter of the orbit with E=0 and l^2 = 16 G^2 M_bullet^2 / c^2
    # rlc = 8*G*M/c^2
    rlc = 4*G*M/c^2

    Elc = rlc*phi_p(rlc)/2 + phi(rlc)	# Energy of the circular orbit
    slc = log(1+Elc/E0);							

    l2_lc(E)  = 2*rlc^2*(phi(rlc) - E)	# squared angular momentum of the circular orbit
    
    wlc   = log(1+phi(rlc)/E0)			# maximum value of w, that is its value at rlc		
 
    ## Same for stars
    rsun      = 6.960e8/params["r_u"]
    rtde      = rsun*(M/ms)^(1/3)

    Etde      = rtde*phi_p(rtde)/2 + phi(rtde)
    stde   = log(1+Etde/E0);

    l2_tde(E) = 2*rtde^2*(phi(rtde) - E)

    wtde   = log(1+phi(rtde)/E0);

     #Central points of the grid
    s_val = slc/Ngrids*collect( 0.5:1:Ngrids )
    R_val = 1/NgridR*collect( 0.5:1:NgridR )
    
    rmax = 1e20*rh; # To bound fzero into an interval

    #Store relevant quantities
    # The initial distribution function is the isotropic one manually set to zero
    # below the loss cone curve, that is the locus of E and l2 of the orbits with
    # pericenter at rlc
    
    fs       = zeros(Ngrids);     # the isotropic function
    fsbh     = zeros(Ngrids);     # the isotropic function
    rc       = zeros(Ngrids);     # circular radius at s_j
    l2c      = zeros(Ngrids);     # l2 at that orbit
    wc       = zeros(Ngrids);     # w at rc
    Rlc      = zeros(Ngrids);     # loss cone R at s_j
    Rtde     = zeros(Ngrids);     # TDE loss cone at R at s_j
    Rlc_idx  = zeros(Int,Ngrids); # index of the first R_val above R_lc[j] 
    Rtde_idx = zeros(Int,Ngrids); # index of the first R_val above R_lc[j] 
    
    ##Integration for Abel transform:
    # Gauss-Legendre quadrature with params["N_coeffs"] points
    
    (pt,wg) = gausslegendre(params["N_coeffs"])
    prefact_f = sqrt(2*E0)/4/pi^2      #%sqrt(2)/4/pi^2/sqrt(5)*sigma / (10^5 / (2*pi*sigma)^1.5)

    function Integrand_f(s,w)
        r = fzero(r-> w - log(1+phi(r)/E0), [rlc, rmax], xreltol=1e-12)
        return exp(w)*n_ps(r)/sqrt(exp(s) - exp(w))
    end
    function Integrand_fbh(s,w)
        r = fzero(r-> w - log(1+phi(r)/E0), [rlc, rmax], xreltol=1e-12)
        return exp(w)*nbh_ps(r)/sqrt(exp(s) - exp(w))
    end

    u0 = 1.0
    for i=1:Ngrids
        pts   = scale_p.(0.,s_val[i],pt)
        wgs   = scale_w.(0.,s_val[i],wg)
        fs[i]   = prefact_f*wgs'* Integrand_f.(s_val[i], pts)
        fsbh[i] = prefact_f*wgs'* Integrand_fbh.(s_val[i], pts)

        E_tmp       = E0*(exp(s_val[i]) - 1)
        tozero      = r-> E_tmp - r.*phi_p(r)/2 - phi(r)
        rc[i]       = exp(fzero(u->tozero(exp(u)),u0, atol=0.0, rtol=0.0));
        wc[i]       = log(1+5*phi(rc[i]) / sigma^2)
        l2c[i]      = -rc[i]^3*phi_p(rc[i]);
        Rlc[i]      = l2_lc(E_tmp)/l2c[i];
        Rtde[i]     = l2_tde(E_tmp)/l2c[i]; # Be careful: after stde, rtde is an apocenter!
        t           = findfirst(R_val .> Rlc[i])
        Rlc_idx[i]  = isnothing(t) ? NgridR : findfirst(R_val .> Rlc[i])
        t           = findfirst(R_val .> Rtde[i])
        Rtde_idx[i] = isnothing(t) ? NgridR : findfirst(R_val .> Rtde[i])
        u0 = log(rc[i])
    end
    
    f = zeros(NgridR,Ngrids,2); #from fs(s) one computes the intial distributions in (s,R) space
    idx_max_tde = findlast(s_val .<= stde) #Since the tde radius is usually smaller than the EMRI
                                           # loss cone radius, this is the index in s_val that 
                                           # corresponds to the last usable value
                                           
    #Rough estimation of the loss-cone: 0 or 1
    loss_cone = R_val .> Rlc'
    if haskey(params, "stars_lc") && params["stars_lc"] == "tde"
        tde_loss_cone = (R_val .> Rtde') .& (s_val' .<= stde)
    else
        tde_loss_cone = loss_cone
        Rtde = Rlc
        Rtde_idx = Rlc_idx
        idx_max_tde = Ngrids
        stde=slc
        rtde=rlc
    end

    edge         = zeros(NgridR, Ngrids)
    edge_tde     = zeros(NgridR, Ngrids)

    for j = 1:Ngrids
    	Rdof_idx_j = 0
    	Rdof_idx_tde_j = 0
    	
        if j == Ngrids
            Rdof_idx_j = Rlc_idx[j] + 1
            Rdof_idx_tde_j = Rtde_idx[j] + 1
        else
            Rdof_idx_j = Rlc_idx[j+1]
            Rdof_idx_tde_j = Rtde_idx[j+1]
        end
        
        edge[ Rlc_idx[j] , j]      = 1
        edge_tde[ Rtde_idx[j] , j] = 1

        for l = Rlc_idx[j]:(Rdof_idx_j-1)
            edge[l, j]      = 1
        end
        
        for l = Rtde_idx[j]:(Rdof_idx_tde_j-1)
            edge_tde[l, j]  = 1
        end
    end

    edge_tde[:, idx_max_tde+1:end] .= 0
    
    if idx_max_tde < Ngrids
        edge_tde[NgridR, idx_max_tde+1] = 1
    end
    
    #Assembly ofthe function
    f[:,:,1] = fs' .* tde_loss_cone
    #The distribution function of sBHs is proportional to that of stars.
    f[:,:,2] = fsbh' .* loss_cone

    
    
    #Save some stuff for later convenience.
    auxiliary=Dict( "phi"           => phi,
                    "phip"          => phi_p,
                    "phis"          => phi_s,
                    "r_p"           => r_p,
                    "rc"            => rc,
                    "l2c"           => l2c,
                    "Rlc"           => Rlc,
                    "Rtde"          => Rtde,
                    "Rlc_idx"       => Rlc_idx,
                    "Rtde_idx"      => Rtde_idx,
                    "wc"            => wc,
                    "s_val"         => s_val,
                    "R_val"         => R_val,
                    "l2_lc"         => l2_lc,
                    "l2_tde"        => l2_tde,
                    "n"             => n,
                    "f_ref"         => fs,
                    "tde_loss_cone" => tde_loss_cone,
                    "loss_cone"     => loss_cone,
                    "edge"          => edge,
                    "edge_tde"      => edge_tde,
                )

    #Save useful parameters
    params["wlc"]  = wlc
    params["slc"]  = slc
    params["wtde"] = wtde
    params["stde"] = stde
    params["rlc"]  = rlc
    params["rtde"] = rtde
    params["rh"]   = rh
    params["E0"]   = sigma^2/5
    params["sGW"]  = log(1+G*M/(0.02*rh)/E0)

    return f, auxiliary
end

"""
	f2AuxFun(f, params, aux)

Computes the auxiliary functions for the computation of the FP coefficients.
	params and aux are usually obtained from IC(). f is an array

Outputs:
	F0 		Ngridw x 2 Array
	F1 		Ngrids x Ngridw x 2 Array
	F2 			""
"""
function f2AuxFun(f::Array{Float64,3}, params::Dict, aux::Dict)
	# Save some local variables
    Ngrids = params["Ngrids"]

    cs    =  (16*pi^2) * 10 * params["E0"] * params["G"]^2 * params["ms"]^2;
    cbh   =  (16*pi^2) * 10 * params["E0"] * params["G"]^2 * params["mbh"]^2;
    ds    = aux["s_val"][2]-aux["s_val"][1]
    dR    = aux["R_val"][2]-aux["R_val"][1]
    s_val = aux["s_val"]
    w_val = [s_val; s_val[end]+ds:ds:params["wlc"]+2ds] #to cover the edges, we prolong w

    # Marginalize over R
    f_avg = dropdims(sum( f ,dims=1), dims=1)*dR
    
    # Create a (linear) interpolant with included edges
    f_s  = LinearInterpolation([0.;s_val;params["slc"]], [0.;f_avg[:,1]; 0.])
    f_bh = LinearInterpolation([0.;s_val;params["slc"]], [0.;f_avg[:,2]; 0.])

    n_extra = length(w_val) - Ngrids            #Number of extra points in w grid

    ### F0 = int_0^E dE' f_avg(E')
    F0      = zeros(Ngrids+n_extra,2)
    (pt,wg) = gausslegendre(params["N_coeffs"])
    F0[1:Ngrids,:] = cumsum(exp.(s_val).*f_avg, dims=1)*ds .* [cs cbh] #It is a simple cumulative integral
    
    #Then the function is constant up to wlc
    for i=1:n_extra
        F0[Ngrids + i,:] = F0[Ngrids,:]
    end

	## F1 = int_0^E dE' f_avg(E') sqrt( (phi - E')/(phi - E) )
	## F1 = int_0^E dE' f_avg(E') sqrt( (phi - E')/(phi - E) )^3
    F1 = zeros(Ngrids, Ngrids + n_extra, 2)
    F2 = zeros(Ngrids, Ngrids + n_extra, 2)
    Threads.@threads for j=1:Ngrids
        for i = j+1:Ngrids # Up to the loss cone, the integrand is non-vanishing
            pts = scale_p.(s_val[j], w_val[i], pt)
            wgs = scale_w.(s_val[j], w_val[i], wg)
            
            integrand1(sp) = exp(sp)*((1-exp(sp-w_val[i]) )/( 1-exp(s_val[j]-w_val[i]) ) )^0.5
            F1[j,i,1] = wgs'*(integrand1.(pts).*f_s.(pts))*cs
            F1[j,i,2] = wgs'*(integrand1.(pts).*f_bh.(pts))*cbh
            
            integrand2(sp) = exp(sp)*((1-exp(sp-w_val[i]) )/( 1-exp(s_val[j]-w_val[i]) ) )^1.5
            F2[j,i,1] = wgs'*(integrand2.(pts).*f_s.(pts))*cs
            F2[j,i,2] = wgs'*(integrand2.(pts).*f_bh.(pts))*cbh
        end
        for i = Ngrids+1:Ngrids+n_extra # But outside of it the integral must stop at Elc.
            pts = scale_p.(s_val[j], params["slc"], pt)
            wgs = scale_w.(s_val[j], params["slc"], wg)
            
            integrand1(sp) = exp(sp)*((1-exp(sp-w_val[i]) )/( 1-exp(s_val[j]-w_val[i]) ) )^0.5
            F1[j,i,1] = wgs'*(integrand1.(pts).*f_s.(pts))*cs
            F1[j,i,2] = wgs'*(integrand1.(pts).*f_bh.(pts))*cbh

            integrand2(sp) = exp(sp)*((1-exp(sp-w_val[i]) )/( 1-exp(s_val[j]-w_val[i]) ) )^1.5
            F2[j,i,1] = wgs'*(integrand2.(pts).*f_s.(pts))*cs
            F2[j,i,2] = wgs'*(integrand2.(pts).*f_bh.(pts))*cbh
        end
    end

    haskey(aux, "w_val") || (aux["w_val"] = w_val) #Store w_val if not stored already
    params["n_extra"] = n_extra
    return F0, F1, F2
end

function Precomputed_objects(params, aux)
    filename = "precomputed_objs_$(params["stars_lc"])_$(params["rlc"])_$(params["sigma"])_$(params["NgridR"])x$(params["Ngrids"])_$(params["N_coeffs"]).jld2"
    if isfile(filename)
        w_gr_stack, W_stack = load(filename, "w_gr_stack","W_stack")
    else
        NgridR = params["NgridR"]
        Ngrids = params["Ngrids"]
        N = params["N_coeffs"]
        E0      = params["E0"]
        rlc      = params["rlc"]
        s_val   = aux["s_val"]
        R_val   = aux["R_val"]
        w_val   = aux["w_val"]
        rc      = aux["rc"]
        l2c     = aux["l2c"]
        wc      = aux["wc"]
        phi     = aux["phi"]
        r_p     = aux["r_p"]
        Rlc_idx = aux["Rlc_idx"]
        v²(w, s)      = 2*E0*(exp(w) - exp(s))
        vt²(R,Jc², r) = Jc² * R / r^2
        vr(w, s, R, Jc²,r) = sqrt(2*E0*(exp(w) - exp(s)) - Jc² * R / r^2)

        w_r   = r-> log(1 + phi(r)/E0)
        cpts = chebpts(N)
        rmax   = 10^7*params["rh"]

        w_gr_stack = zeros(Ngrids, NgridR, N)
        W_stack    = zeros(6, Ngrids, NgridR, N)

        Threads.@threads for j = 1:Ngrids
            sj = s_val[j]
            Ej = E0*(exp(sj) - 1)

            for l = Rlc_idx[j]:NgridR
                Rl = R_val[l]
                tozero = r-> 2r^2*(phi(r) - Ej) - l2c[j]*Rl
                rminus=0.0
                rminus = exp(fzero( u->tozero(exp(u)), [log(0.5*rlc), log(rc[j])], xrtol=1e-12))
                rplus  = exp(fzero( u->tozero(exp(u)), [log(rc[j]), log(rmax)], xrtol=1e-12))
                wminus = w_r(rminus)
                wplus  = w_r(rplus)
                w_gr   = scale_p.(wplus, wminus, cpts)
                w_gr_stack[j, l, :] = w_gr
                #Compute useful quantities on the Chebyshev grid of the domain
                r_w     = w -> fzero(r-> w - w_r(r), [rminus,rplus], xrtol=1e-12)
                r_gr    = r_w.(w_gr)
                v²_gr   = v².(w_gr, sj)
                vt²_gr  = vt².(Rl,l2c[j],r_gr)
                vr_gr   = vr.(w_gr,sj, Rl, l2c[j],r_gr)
                drdw_gr = E0 *exp.(w_gr) .* r_p.(r_gr)

                W_common = π^2 .*sqrt.((w_gr.-wplus).*(wminus.-w_gr)) ./ vr_gr .* drdw_gr

                #WEE
                W_stack[1, j, l, :]  = -  8/3 *      l2c[j]            * W_common  .* v²_gr
                #WE
                W_stack[2, j, l, :]  =    8   *      l2c[j]            * W_common
                #WER
                W_stack[3, j, l, :]  = - 16/3 * Rl * l2c[j]            * W_common  .* (v²_gr/l2c[j]*rc[j]^2 .- 1)
                #WR
                W_stack[4, j, l, :]  =   16   * Rl *         rc[j]^2   * W_common  .* (1 .- l2c[j]/rc[j]^2 ./v²_gr)
                #WRR0
                W_stack[5, j, l, :]   = - 32/3 * Rl                     * W_common  .* r_gr.^2 ./ v²_gr .*  ( vt²_gr.*(v²_gr/l2c[j]*rc[j]^2 .- 1).^2 .+ vr_gr.^2 )
                #WRR1
                W_stack[6, j, l, :]  = - 16   * Rl                     * W_common  .* r_gr.^2 ./ v²_gr .* vr_gr.^2
            end
        end
        jldsave(filename; w_gr_stack, W_stack)
    end
    
    return w_gr_stack, W_stack
end

function AuxFun2coeffs(F0::Array{Float64,2}, F1::Array{Float64,3}, F2::Array{Float64,3}, params::Dict, aux::Dict)
    NgridR = params["NgridR"]
    Ngrids = params["Ngrids"]
    N = params["N_coeffs"]
    mbh     = params["mbh"]
    ms      = params["ms"]
    Rlc_idx = aux["Rlc_idx"]
    w_val   = aux["w_val"]
    w_gr_stack, W_stack = Precomputed_objects(params, aux)
    
    DEE = zeros(NgridR, Ngrids)
    DER = zeros(NgridR, Ngrids)
    DE  = zeros(NgridR, Ngrids)
    DR  = zeros(NgridR, Ngrids)
    DRR = zeros(NgridR, Ngrids)
    C   = zeros(NgridR, Ngrids)
    
    Threads.@threads for j = 1:Ngrids
        for l = Rlc_idx[j]:NgridR

            w_gr   = w_gr_stack[j,l,:]

            WEE  = W_stack[1, j, l, :]
            WE   = W_stack[2, j, l, :]
            WER  = W_stack[3, j, l, :]
            WR   = W_stack[4, j, l, :]
            WRR0 = W_stack[5, j, l, :]
            WRR1 = W_stack[6, j, l, :]
            
            F0s = F0[j,1]
            F0bh = F0[j,2]
            F1s  = LinearInterpolation([0.0; w_val], [0.0;F1[j,:,1]])
            F1bh = LinearInterpolation([0.0; w_val], [0.0;F1[j,:,2]])
            F2s  = LinearInterpolation([0.0; w_val], [0.0;F2[j,:,1]])
            F2bh = LinearInterpolation([0.0; w_val], [0.0;F2[j,:,2]])

            DEE[l,j] = pi/N * WEE'* (F0s .+ F0bh .+ F2s(w_gr) .+ F2bh(w_gr) )
            DER[l,j] = pi/N * WER'* (F0s .+ F0bh .+ F2s(w_gr) .+ F2bh(w_gr) )
            DE[l,j]  = pi/N * WE' * (F1s(w_gr) .+ ms/mbh * F1bh(w_gr))
            DR[l,j]  = pi/N * WR' * (F1s(w_gr) .+ ms/mbh * F1bh(w_gr))
            DRR[l,j] = pi/N * (sum(WRR0*(F0s + F0bh)) +
                                WRR1'*(F1s(w_gr) + F1bh(w_gr)) +
                                (WRR0 - WRR1)'*(F2s(w_gr) + F2bh(w_gr) ) )
            
            C[l,j]    = - pi/N * sum(WE)
        end
    end

    

    return DEE, DE, DER, DR, DRR, C
end

"""
AB_dof(f,C,DRR, params, aux)
Compute the matrices A,B such that
f^dof = B*f    f = A*f^dof
"""
function AB_dof(f,C,DRR, params, aux)
    NgridR=params["NgridR"]
    Ngrids=params["Ngrids"]
        
    N_dof = Int(sum( aux["loss_cone"] .> 0 ))
    dof_numbering = Int.(copy(aux["loss_cone"]))
    dof_numbering[dof_numbering .> 0] = collect(1:N_dof)
    B = spzeros(N_dof, params["NgridR"]*params["Ngrids"])
    for i = 1:params["NgridR"]*params["Ngrids"]
        if aux["loss_cone"][i] == 1
            B[dof_numbering[i], i] = 1
        end
    end


    #EMRI loss cone
    if aux["Rlc_idx"][end] == params["NgridR"]+1
        P = 1 ./ aux["l2c"][1:end-1] ./4π^2
        D = [DRR[aux["Rlc_idx"][i], i] for i=1:Ngrids-1] ./ aux["R_val"][aux["Rlc_idx"][1:end-1]]
        q = P.*D ./ aux["Rlc"][1:Ngrids-1]
    else
        P = 1 ./ aux["l2c"][1:end] ./4π^2
        D = [DRR[aux["Rlc_idx"][i], i] for i=1:Ngrids] ./ aux["R_val"][aux["Rlc_idx"][1:end]]
        q = P.*D ./ aux["Rlc"][1:Ngrids]
    end
    
    parab(x, xv) = -((x-xv)^2 - xv^2)/xv^2

    for j=1:findlast(aux["Rlc_idx"] .== 1)
        q[j] *= parab(aux["Rlc"][j], aux["R_val"][1])
    end

    lR0 = log.(aux["Rlc"][1:length(q)]) - (q.^2 + q.^4).^0.25
    qbh = copy(q)
    A = spzeros(params["NgridR"]*params["Ngrids"],N_dof)
    for j = 1:Ngrids
        for l = 1:NgridR
            idx = (j-1)*NgridR + l
            if aux["loss_cone"][idx] == 1
                    A[idx, dof_numbering[idx]] = 1
            end
        end
    end


    if haskey(params, "stars_lc") && params["stars_lc"] == "tde"
        #Now TDEs
        #For stars
        N_dofs = Int(sum( aux["tde_loss_cone"] .> 0 ))
        dof_numbering = Int.(copy(aux["tde_loss_cone"]))
        dof_numbering[dof_numbering .> 0] = collect(1:N_dofs)
        Bs = spzeros(N_dofs, params["NgridR"]*params["Ngrids"])
        for i = 1:params["NgridR"]*params["Ngrids"]
            if aux["tde_loss_cone"][i] == 1
                Bs[dof_numbering[i], i] = 1
            end
        end

        idx_max_tde=findlast(aux["s_val"] .<= params["stde"])

        P = 1 ./ aux["l2c"][1:idx_max_tde] ./4π^2
        D = [DRR[aux["Rtde_idx"][i], i] for i=1:idx_max_tde] ./ aux["R_val"][aux["Rtde_idx"][1:idx_max_tde]]
        q = P.*D ./  aux["Rtde"][1:idx_max_tde]
        # parab(x, xv) = -((x-xv)^2 - xv^2)/xv^2

        for j=1:(findfirst(aux["Rtde_idx"] .> 1)-1)
            q[j] *= parab(aux["Rtde"][j], aux["R_val"][1])
        end
        lR0s = log.(aux["Rtde"][1:length(q)]) - (q.^2 + q.^4).^0.25

        As = spzeros(params["NgridR"]*params["Ngrids"],N_dofs)
        for j = 1:idx_max_tde
            for l = 1:NgridR
                idx = (j-1)*NgridR + l
                if aux["tde_loss_cone"][idx] == 1
                        As[idx, dof_numbering[idx]] = 1
                elseif aux["loss_cone"][idx] == 1
                        idx_dof = idx
                        while idx_dof <= length(dof_numbering) && dof_numbering[idx_dof] == 0
                            idx_dof += 1
                        end
                        if log(aux["R_val"][l]) > lR0s[j] &&  dof_numbering[idx_dof] != 0
                            As[idx, dof_numbering[idx_dof]] = (log(aux["R_val"][l]) - lR0s[j])/(log(aux["R_val"][aux["Rtde_idx"][j]]) - lR0s[j])
                        end
                end
            end
        end
    else
        lR0s=lR0
        As=A
        Bs=B
    end
    aux["lR0"] = lR0
    aux["lR0s"] = lR0s
    aux["q"] = qbh
    aux["qs"] = q
    return GBMatrix(dropzeros(A)), GBMatrix(dropzeros(B)), 
           GBMatrix(dropzeros(As)), GBMatrix(dropzeros(Bs))
end

"""
Finite differences matrices (1st and 2nd order) of 2D functions.
They act on linearized functions of two variables.

Normalization:
    # - divide Ds  by ds
"""
function DiffMat2D_fc(NgridR, Ngrids)

    Ds = spzeros(Ngrids+1, Ngrids)
    #Dirichelet at the edges
    #left:zero
    Ds[1,1] = 2
    for j = 1:Ngrids-1
        Ds[j+1, j] = -1
        Ds[j+1, j+1] =  1
    end
    Ds[Ngrids+1,Ngrids] = -2
    dsE = kron(Ds,sparse(I, NgridR, NgridR))
    
    Ds = spzeros(Ngrids, Ngrids+1)
    #Dirichelet at the edges
    #left:zero
    for j = 1:Ngrids
        Ds[j, j] = -1
        Ds[j, j+1] =  1
    end
    DFE = kron(Ds,sparse(I, NgridR, NgridR))
    
    Ds = spzeros(Ngrids, Ngrids)
    Ds[1,2] = 2/3
    for j = 2:Ngrids-1
        Ds[j, j-1] = -0.5
        Ds[j, j+1] =  0.5
    end
    Ds[Ngrids,Ngrids-1] = -2/3


    dsR = kron(Ds,sparse(I,NgridR, NgridR))

    ##R derivatives

    DR = spzeros(NgridR+1, NgridR)
    #Dirichelet at the edges
    DR[1,1] = -1
    DR[1,2] = 1
    for j = 1:NgridR-1
        DR[j+1, j] = -1
        DR[j+1, j+1] =  1
    end
    #top:zero.

    dRR = kron(sparse(I, Ngrids, Ngrids), DR)

    DR = spzeros(NgridR, NgridR+1)
    #Dirichelet at the edges

    for j = 1:NgridR-1
        DR[j, j] = -1
        DR[j, j+1] =  1
    end
    DR[NgridR, NgridR] = -1.0

    DFR = kron(sparse(I, Ngrids, Ngrids), DR)
    
    DR = spzeros(NgridR, NgridR)
    DR[1,1] = -1
    DR[1,2] = 1
    for j = 2:NgridR-1
        DR[j, j-1] = -0.5
        DR[j, j+1] =  0.5
    end
    DR[NgridR,NgridR-1] = -1
    DR[NgridR,NgridR] = 1
    dRE = kron(sparse(I,Ngrids, Ngrids), DR)


    return GBMatrix(DFE), GBMatrix(DFR), 
            GBMatrix(dsE), GBMatrix(dsR),
            GBMatrix(dRE), GBMatrix(dRR)
end

"""
RHS_Mat(DEE, DE, DER, DR, DRR, C, params, aux)

Computes the matrices Ms and Mbh such that the orbit averaged
FP equation can be written as

d/dt f_s = M_s * f_s
d/dt f_bh = M_s * f_bh

where f_s and f_bh are linearized.
"""
function RHS_Mat_fc(DEE, DE, DER, DR, DRR, C, f_loc, params, aux)
    Ngrids=params["Ngrids"]
    NgridR=params["NgridR"]
    ds = aux["s_val"][2] - aux["s_val"][1]
    dR = aux["R_val"][2] - aux["R_val"][1]
    
    DFs     = copy(aux["DFs"])
    DFR     = copy(aux["DFR"])
    dsE     = copy(aux["dsE"])
    dsR     = copy(aux["dsR"])
    dRE     = copy(aux["dRE"])
    dRR     = copy(aux["dRR"])
    f_forFR = copy(aux["f_forFR"])
    f_forFE = copy(aux["f_forFE"])
    f_forFR_s = copy(f_forFR)
    f_forFE_s = f_forFE

    dRR_s = copy(dRR)
    dRE_s = copy(dRE)
    dsR_s = copy(dsR)

    ds_dE = ( (1/params["E0"] * (exp.(-aux["s_val"])'.*
                ( f_loc[:,:,2] .> 0 .|| f_loc[:,:,1] .> 0))[:])) 

    ##Boundary conditions
    
    ##Left edge: OK: Dirichelet conditions in the derivative of f

    ##Upper edge: Dirichelet conditions in the derivative of FE

    ##Lower edge: Needs specific treatment

    #1. Get A,B, lR0
    A,B,As,Bs = AB_dof(f_loc,C, DRR, params, aux)
    lR0  = aux["lR0"]
    lR0s = aux["lR0s"]

    #EMRIs
    dRR[1,:] .= 0
    dRR[1,1] =  1/(log(aux["R_val"][aux["Rlc_idx"][1]]) - lR0[1]) / aux["R_val"][aux["Rlc_idx"][1]]
    dRR_s[1,:] .= 0
    dRR_s[1,1] =  1/(log(aux["R_val"][aux["Rtde_idx"][1]]) - lR0s[1]) / aux["R_val"][aux["Rtde_idx"][1]]
    for j=2:findlast(aux["Rlc_idx"] .<= NgridR)
        R_v = aux["R_val"][aux["Rlc_idx"][j]]
        idx = (j-1)NgridR + aux["Rlc_idx"][j]

        dRR[idx + (j-1), :]  .= 0
        dRR[idx + (j-1), idx] = 1/(log(R_v) - lR0[j]) / R_v
        
        dRE[idx, :]  .= 0
        dRE[idx, idx] = 0.5/(log(R_v) - lR0[j]) / R_v
        if idx < NgridR*(Ngrids-1)  #If it is not at the last cell in s
            mod(idx, Ngrids) == Ngrids-1 || (dRE[idx, idx+NgridR + 1] = 0.25/dR)
            mod(idx, Ngrids) == 0 || (dRE[idx, idx+NgridR - 1] = -0.25/dR)
        end
        
        f_forFR[(j-1)*(NgridR+1) + aux["Rlc_idx"][j], :] .= 0
        f_forFR[(j-1)*(NgridR+1) + aux["Rlc_idx"][j], idx] = 1
        
        #TDEs
        if aux["s_val"][j] < params["stde"]
            R_v = aux["R_val"][aux["Rtde_idx"][j]]
            idx = (j-1)NgridR + aux["Rtde_idx"][j]
        
            dRR_s[idx + (j-1), :]  .= 0
            dRR_s[idx + (j-1), idx] = 1/(log(R_v) - lR0s[j]) / R_v
            
            dRE_s[idx, :]  .= 0
            dRE_s[idx, idx] = 0.5/(log(R_v) - lR0s[j]) / R_v
            if idx <= (NgridR-1)*(Ngrids-1)
                dRE_s[idx, idx+NgridR + 1] = 0.25/dR
                dRE_s[idx, idx+NgridR - 1] = -0.25/dR
            end
            
            f_forFR_s[(j-1)*(NgridR+1) + aux["Rtde_idx"][j], :] .= 0
            f_forFR_s[(j-1)*(NgridR+1) + aux["Rtde_idx"][j], idx] = 1
        end
    end

    f_forFE = dropzeros(f_forFE)
    f_forFR = dropzeros(f_forFR)
    f_forFE_s = dropzeros(f_forFE_s)
    f_forFR_s = dropzeros(f_forFR_s)
    f_forFR2 = copy(f_forFR)
    f_forFR_s2 = copy(f_forFR)
    parab(x, xv) = -((x-xv)^2 - xv^2)/xv^2

    for j=1:findlast(aux["Rlc_idx"] .== 1)
        f_forFR[(NgridR+1)*(j-1) + 1, (NgridR)*j+1] = parab(aux["Rlc"][j], aux["R_val"][1])
        (aux["Rtde_idx"][j] == 1) && (f_forFR_s[(NgridR+1)*(j-1) + 1, (NgridR)*j+1] = parab(aux["Rtde"][j], aux["R_val"][1]))
    end
    # println(findlast(aux["Rlc_idx"] .== 1))
    dRE = dropzeros(dRE)
    dRE_s = dropzeros(dRE_s)
    dRR = dropzeros(dRR)
    dRR_s = dropzeros(dRR_s)
    
    dRE = f_forFE*dRE
    dsR = f_forFR2*dsR

    dRE_s = f_forFE_s*dRE_s
    dsR_s = f_forFR_s2*dsR_s

    #Construct the fluxes:
    sided(x) = GBMatrix(spdiagm(x))
    MFe = sided(f_forFE_s*GBVector(DE[:]))*f_forFE_s + sided(f_forFE_s*GBVector( DEE[:].*ds_dE) )*dsE + sided(f_forFE_s*GBVector(DER[:]))*dRE_s
    MFe2 = params["mbh"]/params["ms"]*sided(f_forFE*GBVector(DE[:]))*f_forFE + sided(f_forFE*GBVector(DEE[:].*ds_dE))*dsE + sided(f_forFE*GBVector(DER[:]))*dRE

    MFR = sided(f_forFR_s*GBVector(DR[:]))*f_forFR_s2 + sided(f_forFR_s*GBVector(DER[:].*ds_dE))*dsR_s + sided(f_forFR_s*GBVector(DRR[:]))*dRR_s
    MFR2 = params["mbh"]/params["ms"]*sided(f_forFR*GBVector(DR[:]))*f_forFR2 + sided(f_forFR*GBVector(DER[:].*ds_dE))*dsR + sided(f_forFR*GBVector(DRR[:]))*dRR
    
    out1 = sided(ds_dE)*(DFs*MFe)  + DFR*MFR
    out2 = sided(ds_dE)*(DFs*MFe2) + DFR*MFR2
    

    return MFe, MFR, MFe2, MFR2, out1, out2
end

"""
evolution_implicit(f, params,aux, t_v,N; N_substeps=1, epsilon=1)

Computes N_substeps in the evolution of the FP equation with an implit scheme.

d/dt f_s = M_s * f^next_s
d/dt f_bh = M_s * f^next_bh

where f_s and f_bh are linearized.
"""
function flux_conservative(f, params,aux, t_v)
    N = params["N_steps"]
    N_substeps= params["N_substeps"]
    epsilon=params["epsilon"]
    N_save=params["N_save"]
    
    
    NgridR = params["NgridR"]
    Ngrids = params["Ngrids"]
    Rlc_idx = aux["Rlc_idx"]
    ds = aux["s_val"][2] - aux["s_val"][1]
    dR = aux["R_val"][2] - aux["R_val"][1]

    f_evol = copy(f)
    
    GW_idx = Int(findfirst(aux["s_val"] .>= params["sGW"])) #it has an additional point at the beginning
    edge_EMRI   = (Bool.(aux["edge"]) .& (aux["s_val"]' .> (params["sGW"] - ds)))[:]
    edge_s_EMRI = (Bool.(aux["edge_tde"]) .& (aux["s_val"]' .> (params["sGW"] - ds)))[:]
    edge_tot   = (Bool.(aux["edge"]) .& (aux["s_val"]' .> (-1.0)))[:]
    edge_s_tot = (Bool.(aux["edge_tde"]) .& (aux["s_val"]' .> (-1.0)))[:]
    j      = kron(1:Ngrids,ones(NgridR))
    l      = kron(ones(Ngrids),1:NgridR)
    j_edge_EMRI = Int.(j[edge_EMRI])
    j_edge_s_EMRI = Int.(j[edge_s_EMRI])
    l_edge_EMRI = Int.(l[edge_EMRI])
    l_edge_s_EMRI = Int.(l[edge_s_EMRI])
    j_edge_tot = Int.(j[edge_tot])
    j_edge_s_tot = Int.(j[edge_s_tot])
    l_edge_tot = Int.(l[edge_tot])
    l_edge_s_tot = Int.(l[edge_s_tot])

    Gamma = zeros(N*N_substeps)
    Gamma_EGW = zeros(N*N_substeps)
    N_inside  = zeros(N*N_substeps)
    N_tot = zeros(N*N_substeps)
    
    Gamma_s = zeros(N*N_substeps)
    Gamma_EGW_s = zeros(N*N_substeps)
    N_inside_s  = zeros(N*N_substeps)
    N_tot_s = zeros(N*N_substeps)
    
    t = zeros(N*N_substeps)
    Gamma_max = 0
    t_Gamma_max = 0
    dt=0
    
    file_name = params["filename"]
    file_idx = 0

    if N_save > 0
        str_idx = @sprintf("%03d",file_idx)
        curr_file_name = file_name*str_idx*".jld2"
        save(curr_file_name,Dict("params"=>params,
                                "aux"=>aux,
                                "f"=> f_evol))
        file_idx += 1
    end
    i=1
    has_been_positive = 0
    Ten_tE = false
    
    
    while i <= N &&
        (params["fin_cond"] != "time" || t_v < params["t_fin"]) &&
        (params["fin_cond"] != "Pan&Yang" || (i<=3 || !( N_inside[N_substeps*(i-1)] < N_inside[N_substeps*(i-1) - 1] && N_inside[N_substeps*(i-1)-1] >= N_inside[N_substeps*(i-1) - 2] ) ) ) &&
        (i<=3 || dt/t_v > 1e-8) &&
        (~Ten_tE)
        
        MFE_s, MFR_s, MFE, MFR, Ms, Mbh, A, B, A_s, B_s, C = 
        let (F0, F1, F2)                       = f2AuxFun(f_evol,params, aux)
            (DEE, DE, DER, DR, DRR, C)         = AuxFun2coeffs(F0, F1, F2, params, aux)

            MFE_s, MFR_s, MFE, MFR, Ms, Mbh  = RHS_Mat_fc(DEE,DE,DER,DR,DRR,C,f_evol,params, aux)
            A, B, A_s, B_s = AB_dof(f_evol, C, DRR, params, aux)
            MFE_s, MFR_s, MFE, MFR, Ms, Mbh, A, B, A_s, B_s, C
        end
        
        
        let tmp = findnz(Ms)
            for (row, col, v) in zip(tmp[1], tmp[2], tmp[3])
                Ms[row, col] = v/C[row]
            end
        end
        Ms = B_s * (Ms * A_s)
        
        let tmp = findnz(Mbh)
            for (row, col, v) in zip(tmp[1], tmp[2], tmp[3])
                Mbh[row, col] = v/C[row]
            end
        end
        Mbh = B*(Mbh*A)
        
        GBMatrix2Sparse(x) = (let tmp = findnz(x) # return to SparseMatrixCSC
                                sparse(Int64.(tmp[1]), Int64.(tmp[2]), Float64.(tmp[3]), size(x)[1], size(x)[2])
                              end)
        Mbh = GBMatrix2Sparse(Mbh)
        Ms  = GBMatrix2Sparse(Ms)
        A   = GBMatrix2Sparse(A)
        B   = GBMatrix2Sparse(B)
        A_s = GBMatrix2Sparse(A_s)
        B_s = GBMatrix2Sparse(B_s)
        
        dt = epsilon*minimum(abs.((B_s*f_evol[:,:,1][:]) ./ ((Ms*B_s)*f_evol[:,:,1][:])))
        dt = min(dt,epsilon*minimum(abs.((B*f_evol[:,:,2][:]) ./ ((Mbh*B)*f_evol[:,:,2][:]))))
        
        for j=1:N_substeps
            fs  =   B_s*f_evol[:,:,1][:]
            fbh  =  B*f_evol[:,:,2][:]

            if params["scheme"] == 2 #implicit
                fs = A_s*((I - dt*Ms )\fs)
                fbh = A*((I - dt*Mbh)\fbh)
                f_evol[:,:,1] = fs
                f_evol[:,:,2] = fbh
                #Compute the flux towards the loss_cone
                FE   = -reshape(MFE*GBVector(f_evol[:,:,2][:]), NgridR, Ngrids+1)
                FR   = -reshape(MFR*GBVector(f_evol[:,:,2][:]), NgridR+1, Ngrids)
                FE_s = -reshape(MFE_s*GBVector(f_evol[:,:,1][:]), NgridR, Ngrids+1)
                FR_s = -reshape(MFR_s*GBVector(f_evol[:,:,1][:]), NgridR+1, Ngrids)

            elseif params["scheme"] == 1  #explicit
                FE   = -reshape(MFE*GBVector(f_evol[:,:,2][:]), NgridR, Ngrids+1)
                FR   = -reshape(MFR*GBVector(f_evol[:,:,2][:]), NgridR+1, Ngrids)
                FE_s = -reshape(MFE_s*GBVector(f_evol[:,:,1][:]), NgridR, Ngrids+1)
                FR_s = -reshape(MFR_s*GBVector(f_evol[:,:,1][:]), NgridR+1, Ngrids)

                f_evol[:,:,1] += reshape(dt*A*Ms *fs, NgridR, Ngrids)
                f_evol[:,:,2] += reshape(dt*A*Mbh*fbh, NgridR, Ngrids)
            end
            
            #F
            vals = zeros(length(j_edge_EMRI))
            Threads.@threads for k in 2:length(j_edge_EMRI) #The first is needed to check the type of the edge
                if k==length(j_edge_EMRI) || l_edge_EMRI[k+1] > l_edge_EMRI[k] #on the right is empty
                    vals[k]   = FE[l_edge_EMRI[k],j_edge_EMRI[k]+1]*dR
                end
                if j_edge_EMRI[k-1] < j_edge_EMRI[k] #below is empty
                    vals[k]   = -FR[l_edge_EMRI[k],j_edge_EMRI[k]]*params["E0"]*exp(aux["s_val"][j_edge_EMRI[k]])*ds
                end
            end
            Gamma[j + (i-1)*N_substeps] = sum(vals)
            
            vals = zeros(length(j_edge_s_EMRI))
            Threads.@threads for k in 2:length(j_edge_s_EMRI) #The first is needed to check the type of the edge_s
                if k==length(j_edge_s_EMRI) || l_edge_s_EMRI[k+1] > l_edge_s_EMRI[k] #on the right is empty
                   vals[k]   = FE_s[l_edge_s_EMRI[k],j_edge_s_EMRI[k]+1]*dR
                end
                if j_edge_s_EMRI[k-1] < j_edge_s_EMRI[k] #below is empty
                   vals[k]   = -FR_s[l_edge_s_EMRI[k],j_edge_s_EMRI[k]]*params["E0"]*exp(aux["s_val"][j_edge_s_EMRI[k]])*ds
                end
            end
            Gamma_s[j + (i-1)*N_substeps] = sum(vals)
            
            #Compute the flux of fbh towards the E=EGW line
            Gamma_EGW[j + (i-1)*N_substeps]   = sum(FE[:, GW_idx])*dR
            Gamma_EGW_s[j + (i-1)*N_substeps] = sum(FE_s[:, GW_idx])*dR
            

            #Compute the number of blackholes at E>EGW
            N_inside[j + (i-1)*N_substeps]   = (sum( (f_evol[:,:,2].*C.*exp.(aux["s_val"])')[:,GW_idx:end]*params["E0"])*ds*dR)
            N_inside_s[j + (i-1)*N_substeps] = (sum( (f_evol[:,:,1].*C.*exp.(aux["s_val"])')[:,GW_idx:end]*params["E0"])*ds*dR)
            
            N_tot[j + (i-1)*N_substeps]   = (sum( (f_evol[:,:,2].*C.*exp.(aux["s_val"])')*params["E0"])*ds*dR)
            N_tot_s[j + (i-1)*N_substeps] = (sum( (f_evol[:,:,1].*C.*exp.(aux["s_val"])')*params["E0"])*ds*dR)
            
            
            t_v += dt
            t[j + (i-1)*N_substeps] = t_v

            println()
            println("Step $i of $N, substep $j of $N_substeps; t = $(t_v/params["Gyr"]) Gyr")
            flush(stdout)
            if N_save > 0 && mod(j + (i-1)*N_substeps, N_save) == 0
                print("Saving output...")
                str_idx = @sprintf("%03d",file_idx)
                curr_file_name = file_name*str_idx*".jld2"
                t_idx = j + (i-1)*N_substeps

                #Store the differential rate
                vals = zeros(Ngrids)
                Threads.@threads for k in 2:length(j_edge_EMRI) #The first is needed to check the type of the edge_s
                    if k==length(j_edge_tot) || l_edge_tot[k+1] > l_edge_tot[k] #on the right is empty
                        vals[j_edge_tot[k]]   += FE[l_edge_tot[k],j_edge_tot[k]+1]*dR/(params["E0"]*exp(aux["s_val"][j_edge_tot[k]])*ds)
                    end
                    if j_edge_tot[k-1] < j_edge_tot[k] #below is empty
                        vals[j_edge_tot[k]]   += -FR[l_edge_tot[k],j_edge_tot[k]]
                    end
                end

                vals_s = zeros(Ngrids)
                Threads.@threads for k in 2:length(j_edge_EMRI) #The first is needed to check the type of the edge_s
                    if k==length(j_edge_s_tot) || l_edge_s_tot[k+1] > l_edge_s_tot[k] #on the right is empty
                        vals_s[j_edge_s_tot[k]]   = FE_s[l_edge_s_tot[k],j_edge_s_tot[k]+1]*dR/(params["E0"]*exp(aux["s_val"][j_edge_s_tot[k]])*ds)
                    end
                    if j_edge_s_tot[k-1] < j_edge_s_tot[k] #below is empty
                        vals_s[j_edge_s_tot[k]]   = -FR_s[l_edge_s_tot[k],j_edge_s_tot[k]]
                    end
                end

                save(curr_file_name,Dict("t_e"=>t_v,
                                        "f_e"=>f_evol,
                                        "t_gamma"=> t[t_idx-N_save+1:t_idx],
                                        "gamma"=> Gamma[t_idx-N_save+1:t_idx],
                                        "gamma_s"=> Gamma_s[t_idx-N_save+1:t_idx],
                                        "gamma_egw"=> Gamma_EGW[t_idx-N_save+1:t_idx],
                                        "gamma_egw_s"=> Gamma_EGW_s[t_idx-N_save+1:t_idx],
                                        "n_inside" => N_inside[t_idx-N_save+1:t_idx],
                                        "n_inside_s" => N_inside_s[t_idx-N_save+1:t_idx],
                                        "n_tot" => N_tot[t_idx-N_save+1:t_idx],
                                        "n_tot_s" => N_tot_s[t_idx-N_save+1:t_idx],
                                        "diff_rate" => vals,
                                        "diff_rate_s"=> vals_s,
                                        "q"=>aux["q"],
                                        "qs"=>aux["qs"],
                                        )
                )
                file_idx += 1
            end
        end
        aux["C"] = C
        
        if i == 1
            t_Gamma_max = t_v
            Gamma_max = Gamma[N_substeps]
        elseif has_been_positive < 3
            if Gamma[(i-1)*N_substeps + 1] >= Gamma[(i-2)*N_substeps + 1]
                has_been_positive += 1
            else
                has_been_positive = 0
            end
            t_Gamma_max = t_v
            Gamma_max = Gamma[(i-1)*N_substeps+1]
        else
            if Gamma[(i-1)*N_substeps + 1] >= Gamma_max
                t_Gamma_max = t_v
                Gamma_max = Gamma[(i-1)*N_substeps + 1]
            end

            if t_v/t_Gamma_max >= 15
                Ten_tE = true
            end
        end
        i+=1
    end
        println((params["fin_cond"] != "time" || t_v < params["t_fin"]))
        println( (params["fin_cond"] != "Pan&Yang" || (i<=3 || !( N_inside[N_substeps*(i-1)] < N_inside[N_substeps*(i-1) - 1] && N_inside[N_substeps*(i-1)-1] >= N_inside[N_substeps*(i-1) - 2] ) ) ) )
        println((i<=3 || dt/t_v > 1e-7))
        println(~Ten_tE )
    
    return t[1:(i-1)*N_substeps], f_evol, Gamma[1:(i-1)*N_substeps], Gamma_EGW[1:(i-1)*N_substeps], N_inside[1:(i-1)*N_substeps], N_tot[1:(i-1)*N_substeps], Gamma_s[1:(i-1)*N_substeps], Gamma_EGW_s[1:(i-1)*N_substeps], N_inside_s[1:(i-1)*N_substeps], N_tot_s[1:(i-1)*N_substeps]
end


function evolution(f_start, params, aux, t_start)
    if ~haskey(params, "algorithm")
        params["algorithm"] = 1
        println("Defaulting to algorithm = 1 (flux conservative)")
    end
    if ~haskey(params, "scheme")
        params["scheme"] = 1
        println("Defaulting to scheme = 1 (explicit)")
    end

    if ~haskey(params, "algorithn") || params["algorithm"] == 1
        ds = aux["s_val"][2] - aux["s_val"][1]
        dR = aux["R_val"][2] - aux["R_val"][1]
        Ngrids = params["Ngrids"]
        NgridR = params["NgridR"] 
        
        DFs, DFR, dsE, dsR, dRE, dRR = DiffMat2D_fc(NgridR, Ngrids)
        DFs /= ds
        dsR /= ds
        dsE /= ds

        DFR /= dR
        dRE /= dR
        dRR /= dR

        f_forFR = spzeros(NgridR+1, NgridR)
        f_forFR[1,1] = 1
        for i=1:NgridR-1
            f_forFR[i+1,i] = 0.5
            f_forFR[i+1,i+1] = 0.5
        end
        f_forFR[NgridR+1,NgridR] = 1
        f_forFR = GBMatrix(kron(sparse(I, Ngrids, Ngrids), f_forFR))
    
        f_forFE = spzeros(Ngrids+1, Ngrids)
        f_forFE[1,1] = 0.5
        for i=1:Ngrids-1
            f_forFE[i+1,i] = 0.5
            f_forFE[i+1,i+1] = 0.5
        end
        f_forFE[Ngrids+1,Ngrids] = 1
        f_forFE = GBMatrix(kron(f_forFE, sparse(I, NgridR, NgridR)))

        aux["DFs"]     = DFs
        aux["DFR"]     = DFR
        aux["dsE"]     = dsE
        aux["dsR"]     = dsR
        aux["dRE"]     = dRE
        aux["dRR"]     = dRR
        aux["f_forFR"] = f_forFR
        aux["f_forFE"] = f_forFE

        t, f_e, gamma, gamma_egw, n_inside, n_tot, gamma_s, gamma_egw_s, n_inside_s, n_tot_s = flux_conservative(f_start, params, aux, t_start)
        
        save(params["final_output"],Dict("params"=>params,
                         "aux"=>aux,
                         "t"=>t,
                         "f0"=>f_start,
                         "f_e"=>f_e,
                         "gamma"=> gamma,
                         "gamma_egw"=> gamma_egw,
                         "n_inside" =>n_inside,
                         "n_tot" => n_tot,
                         "gamma_s"=> gamma_s,
                         "gamma_egw_s"=> gamma_egw_s,
                         "n_inside_s" =>n_inside_s,
                         "n_tot_s" => n_tot_s))
    else
        error("The only option available is algorithm = 1.")
    end
    
    return t, f_e, gamma, gamma_egw, n_inside, n_tot, gamma_s, gamma_egw_s, n_inside_s, n_tot_s
end
