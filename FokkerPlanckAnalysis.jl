using Roots, FastGaussQuadrature, Printf, FileIO
using Interpolations, LinearAlgebra, SparseArrays
include("MyCheb.jl")

"""
    scale_p(a,b,p)

Linear map of p ∈ [-1,1] to the interval [a,b].
"""
function scale_p(a::Float64,b::Float64,p::Float64)::Float64
    return (a*(1-p) + b*(1+p))/2
end

"""
    scale_w(a,b,w)

Linear map of the Gauss-Legendre points of [-1,1] to those of the interval [a,b].
"""
function scale_w(a::Float64,b::Float64,w::Float64)::Float64
    return (b-a)*w/2
end

"""
    f(2rho, params, aux)
Computes the radial density profile of the components in a FP code.
"""
function f2rho(f, params, aux)
    r_min   = params["rlc"]
    phi = aux["phi"]
    w_pts = chebpts(params["Ngrids"],0., params["wlc"])
    E0=params["E0"]
    minw = aux["s_val"][1]
   # J2c = aux["l2c"]
    R_pts = aux["R_val"]
    Rlc_idx = aux["Rlc_idx"]
    dR = R_pts[2] - R_pts[1]
    ds = w_pts[2] - w_pts[1]
    rc = aux["rc"]
    J2c = extrapolate(interpolate((aux["s_val"],), aux["l2c"],Gridded(Linear())), Line())
    Rlc = extrapolate(interpolate((aux["s_val"],), aux["Rlc"],Gridded(Linear())), Line())

    r = zeros(params["Ngrids"])
    rho = zeros(params["Ngrids"])
    t_f_itp=extrapolate(interpolate((aux["R_val"],aux["s_val"]),f,Gridded(Linear())), 0.)

    function f_itp(R,s)
        y=0
        if s < aux["s_val"][1]
            if R >= aux["R_val"][1] && R <= aux["R_val"][end]
                logy = log(t_f_itp(R,aux["s_val"][1])) + log(s/aux["s_val"][1])*log(t_f_itp(R,aux["s_val"][2])/t_f_itp(R,aux["s_val"][1])) / log(aux["s_val"][2]/aux["s_val"][1])
                y = exp(logy)
            else
                logy = log(t_f_itp(aux["R_val"][1],aux["s_val"][1])) + log(s/aux["s_val"][1])*log(t_f_itp(aux["R_val"][1],aux["s_val"][2])/t_f_itp(aux["R_val"][1],aux["s_val"][1])) / log(aux["s_val"][2]/aux["s_val"][1])
                y = exp(logy)
            end
        else
            y = t_f_itp(R,s)
        end
        return y
    end

    (pt,wg) = gausslegendre(params["N_coeffs"])
    minR=aux["R_val"][1]
    Threads.@threads for i=1:params["Ngrids"]
        r[i] = fzero(r-> log(1 + phi(r)/E0) - w_pts[i],[params["rlc"], 1e20*params["rh"]], xrtol=1e-12)
        p_s = scale_p.(0., min(w_pts[i], params["slc"]), pt)
        w_s = scale_w.(0., min(w_pts[i], params["slc"]), wg)
        left_f(s) = f[1,1]*(f[1,2]/f[1,1])^( log(s/aux["s_val"][1])/log(aux["s_val"][2]/aux["s_val"][1]) )
        for j=1:params["N_coeffs"]
            Rmax = min(2E0*(exp(w_pts[i]) - exp(p_s[j]))/J2c(p_s[j])*r[i]^2,1)

            if Rmax > Rlc(p_s[j])
                p_R = scale_p.(Rlc(p_s[j]), Rmax, pt)
                w_R = scale_w.(Rlc(p_s[j]), Rmax, wg)

                if p_s[j] < aux["s_val"][1]
                    tmp = w_R'*(left_f(p_s[j]) ./ sqrt.(Rmax .- p_R) )
                else
                    tmp = w_R'* (f_itp.(p_R,p_s[j]) ./ sqrt.(Rmax .- p_R) )
                end

                rho[i] += exp(p_s[j])*sqrt(J2c(p_s[j]))*w_s[j]*tmp
            end
        end
#         println("$(Threads.threadid()), $i")
        rho[i] *= 2pi/r[i]*E0
    end

    return rho, r
end

"""
    f(2rho, params, aux)
Computes the radial density profile of the components in a FP code given a function f(s).
"""
function f_ref2rho(f, params, aux)
    r_min   = params["rlc"]
    phi = aux["phi"]
    w_pts = chebpts(params["Ngrids"],0., params["wlc"])
    E0=params["E0"]
    minw = aux["s_val"][1]
   # J2c = aux["l2c"]
    R_pts = aux["R_val"]
    Rlc_idx = aux["Rlc_idx"]
    dR = R_pts[2] - R_pts[1]
    ds = w_pts[2] - w_pts[1]
    rc = aux["rc"]
    J2c = extrapolate(interpolate((aux["s_val"],), aux["l2c"],Gridded(Linear())), Line())
    Rlc = extrapolate(interpolate((aux["s_val"],), aux["Rlc"],Gridded(Linear())), Line())

    r = zeros(params["Ngrids"])
    rho = zeros(params["Ngrids"])
    t_f_itp=extrapolate(interpolate((log.(aux["s_val"]),),log.(aux["f_ref"]),Gridded(Linear())), Line())
    f_itp = s-> exp(t_f_itp(log(s)))
    (pt,wg) = gausslegendre(params["N_coeffs"])
    minR=aux["R_val"][1]
    for i=1:params["Ngrids"]
        r[i] = fzero(r-> log(1 + phi(r)/E0) - w_pts[i],[params["rlc"], 1e20*params["rh"]], xrtol=1e-16)
        p_s = scale_p.(0., w_pts[i], pt)
        w_s = scale_w.(0., w_pts[i], wg)
        Rmax = min.(2*E0*(exp(w_pts[i]) .- exp.(p_s))./J2c(p_s)*r[i]^2,1)

        rho[i] = 4*pi*E0* w_s' * (exp.(p_s) .* f_itp.(p_s) .* sqrt.(2*E0*(exp(w_pts[i]) .- exp.(p_s))))
        #rho[i] = 4*pi*E0* w_s' * (exp.(p_s) .* f_itp.(p_s) .* sqrt.(J2c.(p_s)/ r[i]^2 .* Rmax))
        #rho[i] = -sqrt(2/E0)*pi*w_s' * (exp.(p_s) .* f_itp.(p_s) ./ (exp(w_pts[i]) .- exp.(p_s)).^1.5 )
    end

    return rho, r
end

"""
    give_history(filename_base, variable; interval)
Returns a vector containing the history of "variable" extracted from the outputs beginning with
"filename_base". The default option is to load all the available outputs. A subset can be taken
by setting interval (abstract vector of integers, ideally a range).
"""
function give_history(filename_base, variable; interval=0:-1)
    file_idx = interval.start
    file_idx_m = interval.stop
    str_idx = @sprintf("%03d",file_idx)
    curr_file_name = filename_base*str_idx*".jld2"
    if ( file_idx_m == -1 || file_idx <= file_idx_m) && isfile(curr_file_name)
            output = load(curr_file_name, variable)
    else
       error("File "*curr_file_name*" does not exist.")
    end
    file_idx += 1
    if variable == "f"
            variable = "f_e"
    end
    output_container = Vector{typeof(output)}()
    push!(output_container, output)
    go_on = true
    while go_on == true
        str_idx = @sprintf("%03d",file_idx)
        curr_file_name = filename_base*str_idx*".jld2"
        if ( file_idx_m == -1 || file_idx <= file_idx_m) && isfile(curr_file_name)
            output = load(curr_file_name, variable)
            push!(output_container, output)
            file_idx += 1
        else
            go_on = false
        end
    end
    return output_container
end
