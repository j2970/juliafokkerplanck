"""
    chebpts(N, Inter)

Compute the Chebyshev points of order N of the interval [Inter[1], Inter[2]].
"""
function chebpts(N::Int64)::Array{Float64,1}
    return [cospi((2*j+1)/2/N) for j = N-1:-1:0]
end

function chebpts(N::Int64,Inter::Array{Float64,1})::Array{Float64,1}
    x=chebpts(N)
    return (Inter[2]-Inter[1])/2 .*x .+ (Inter[1]/2 + Inter[2]/2)
end

function chebpts(N::Int64, a::Float64, b::Float64)::Array{Float64,1}
    x=chebpts(N)
    return (b-a)/2*x .+ (b+a)/2
end

"""
    Vals2Coeffs(fj, Nc)

Compute the first Nc Chebyshev coefficients of a func f from its values fj
on a Chebyshev grid.

There is no need to specify the domain. If Nc is not specified, the output has the same length
of the input.
"""
function Vals2Coeffs(fj::Array{Float64,1})::Array{Float64,1}
    N=length(fj)
    c = zeros(N)
    for i=0:N-1
        tmp = i/2/N
        c[i+1] = 2/N*sum([cospi(tmp*(2j+1))*fj[N-j] for j=0:(N-1)])
    end
    c[1]=c[1]/2
    return c
end

function Vals2Coeffs(fj::Array{Float64,1},Nc::Int64)::Array{Float64,1}
    N=length(fj)
    c = zeros(N)
    for i=0:Nc-1
        tmp = i/2/N
        c[i+1] = 2/N*sum([cospi(tmp*(2j+1))*fj[N-j] for j=0:(N-1)])
    end
    c[1]=c[1]/2
    return c
end

"""
    ChebPolyT(k,x)

Evaluate the k-th Chebyshev polynomial of the 1st kind at x.

    ChebPolyT(k)
Returns a callable function to compute at x
"""
function ChebPolyT(k::Int64,x::Float64)::Float64
    return cos(k*acos(x))
end

function ChebPolyT(k::Int64)
    return x-> ChebPolyT(k,x)
end

function ChebPolyT(k::Int64,a::Float64, b::Float64, x::Float64)
    t = 2.0/(b-a)* x - (b+a)/(b-a)
    return ChebPolyT(k,t)
end

function ChebPolyT(k::Int64,Int::Array{Float64,1}, x::Float64)
    t = 2.0/(Int[end]-Int[1])* x - (Int[end]+Int[1])/(Int[end]-Int[1])
    return ChebPolyT(k,t)
end

function ChebPolyT(k::Int64,a::Float64, b::Float64)
    return x-> ChebPolyT(k,a,b,x)
end

function ChebPolyT(k::Int64,Int::Array{Float64,1})
    return x-> ChebPolyT(k,Int[end],Int[1],x)
end

"""
    LinVals2Coeffs(fj, Nc)

Compute the first Nc Chebyshev coefficients of a func f from its values fj
on a unifrom grid.

There is no need to specify the grid domain.
"""
function LinVals2Coeffs(fj::Array{Float64,1},Nc::Int64)::Array{Float64,1}
    th = acos.(collect(LinRange(-1.,1.,length(fj))))
    V = Array{Float64}(undef,length(fj),Nc)
    for k = 0:Nc-1
        V[:,k+1] = cos.(k*th)
    end

    return V\fj
end

"""
    CLinVals2Coeffs(fj, Nc)

Compute the first Nc Chebyshev coefficients of a func f from its values fj
on the central points of a unifrom grid.

There is no need to specify the grid domain.
"""
function CLinVals2Coeffs(fj::Array{Float64,1},Nc::Int64)::Array{Float64,1}
    Nj=length(fj)
    th = acos.(-1.0 .+ 2/Nj*collect( 0.5:1:Nj) )
    V = Array{Float64}(undef,Nc,Nj)
    dth = -diff([pi; 0.5*(th[1:end-1] + th[2:end] ); 0 ] )
    # @infiltrate
    for k = 0:Nc-1
        V[k+1,:] = cos.(k*th).*dth
    end

    return V*fj
end

"""
    Coeffs2Val(cj, x, min, max)

Evaluates the Chebyshev series on [min, max] with coefficients cj at x.
"""
function Coeffs2Val(c::Array{Float64,1},x::Float64)::Float64
    N = length(c)
    th = acos(x)
    y = sum([ c[k+1]* cos(k*th) for k=0:(N-1) ])
    return y
end

function Coeffs2Val(c::Array{Float64,1},x::Float64,min::Float64,max::Float64)::Float64
    x_scaled = 2*x/(max-min) - (max+min)/(max-min)
    return Coeffs2Val(c, x_scaled)
end

function Coeffs2Val(c::Array{Float64,1},x::Array{Float64,1})::Array{Float64,1}
    N = length(c)
    th = acos.(x)
    return  sum(c' .* cos.( (0:N-1)' .*th), dims=2)[:]
end

function Coeffs2Val(c::Array{Float64,1},x::Array{Float64,1},min::Float64,max::Float64)::Array{Float64,1}
    x_scaled = 2*x/(max-min) .- (max+min)/(max-min)
    return  Coeffs2Val(c, x_scaled)
end

"""
    ChebVals2Val(cj, x, min, max)

    Evaluates the Chebyshev series on [min, max] with values fj at x^N_j.
"""
function ChebVals2Val(fj::Array{Float64,1},x::Float64)::Float64
    N = length(fj)
    T = cos(N*acos(x))
    y = (cos(acos(x)*2N) == -1) ? fj[ round(Int, acos(-x)/pi*N+ 0.5) ] :
        sum([ (-1)^(j+N) * T*sinpi((j - 0.5)/N)/N/(x+cospi((j - 0.5)/N)) *fj[j] for j=1:N])
    return y
end

function ChebVals2Val(fj::Array{Float64,1},x::Float64,min::Float64,max::Float64)::Float64
    x_scaled = 2*x/(max-min) - (max+min)/(max-min)
    return ChebVals2Val(fj, x_scaled)
end

function ChebVals2Val(fj::Array{Float64,1},x::Array{Float64,1})::Vector{Float64}
    return [ChebVals2Val(fj, xj) for xj in x]
end

function ChebVals2Val(fj::Array{Float64,1},x::Array{Float64,1},min::Float64,max::Float64)::Vector{Float64}
    x_scaled = 2*x/(max-min) .- (max+min)/(max-min)
    return [ChebVals2Val(fj, xj) for xj in x_scaled]
end
"""
    Fun2Coeffs(min, max, Nc, Nnd, f)

Computes the first Nc Chebyshev coefficients of f on [min, max] by evaluating
it at the Nnd-th order Chebyshev points.
"""
function Fun2Coeffs(min::Float64, max::Float64, ncoeff::Int64, nnodes::Int64, fn::Function)::Array{Float64,1}
    c = zeros(ncoeff)
    f = zeros(nnodes)
    p = zeros(nnodes)
    z = (max + min) / 2
    r = (max - min) / 2
    for k in 0:nnodes-1
        p[k+1] = π * (k + 0.5) / nnodes
        f[k+1] = fn(z + cos(p[k+1]) * r)
    end
    n2 = 2 / nnodes
    for j in 0:ncoeff-1
        s = sum(fk * cos(j * pk) for (fk, pk) in zip(f, p))
        c[j+1] = s * n2
    end
    c[1]=c[1]/2;
    return c
end

"""
    ChangeBasis(c, a, b, A, B)

Computes the Chebyshev series of order N on [A,B] given those on [a,b].
"""

function ChangeBasis(c::Array{Float64,1}, a::Float64, b::Float64, A::Float64, B::Float64)
    N=length(c)
    M_n = zeros(N,N)

    d_1 = B-A;
    d_2 = A+B-b-a;
    d_3 = b-a

    M_n[1,1]=1
    M_n[1,2]= d_2/d_3
    M_n[2,2]= d_1/d_3

    for i = 2:N-1
      M_n[i+1,i+1] = (d_1/d_3)^i
      for j = 0:i-3
          M_n[i-j, i+1] = (d_1*(M_n[i-j-1, i] + M_n[i-j+1,i]) + 2*d_2*M_n[i-j,i])/d_3 - M_n[i-j, i-1]
      end
      M_n[2,i+1] = (d_1*(2*M_n[1,i] + M_n[3,i]) + 2*d_2*M_n[2,i])/d_3 - M_n[2,i-1]
      M_n[1,i+1] = (d_1*M_n[2, i] + 2*d_2*M_n[1,i])/d_3 - M_n[1, i-1]
    end
    
    return M_n*c
end
"""
    ProdChebSeries(a, b)

Computes coefficients of the product of a and b at the same order.
"""
function ProdChebSeries(a::Array{Float64,1}, b::Array{Float64,1})::Array{Float64,1}
    N = length(a)
    if length(b) != N
        error("The coefficients must have the same length!")
    end

    c=zeros(N)

    c[1] += 0.5*(a[1]*b[1] + sum( [ a[l+1] * b[l+1] for l=0:N-1 ] ))

    for j=1:N-1
        c[j+1] += sum( [ a[l+1] * b[j-l+1] for l=0:j ] )
        c[j+1] += sum( [ a[l+1] * b[l-j+1] for l=j:N-1 ] )
        c[j+1] += sum( [ a[l+1] * b[l+j+1] for l=0:N-1-j ] )
        c[j+1] *= 0.5
    end

    return c
end

"""
    Remove_theta(C, d, min, max)

Given the coefficients C in [min, max] of f(x)*θ(x-d)
it tries to extract the coefficients of f in [d, max].
"""
function Remove_theta(C::Array{Float64,1},d::Float64)::Array{Float64,1}
    N=length(C)
    M=zeros(N,N)
    b=Fun2Coeffs(-1., 1., N, N, x->float(x>d))
    
    #j=0 is special
    M[1, 1] += b[1]
    for l=0:N-1
        M[1,l+1] += b[l+1]
    end

    for j=1:N-1
        for l=0:j
            M[j+1,l+1] += b[j-l+1]
        end
        for l=j:N-1
            M[j+1,l+1] += b[l-j+1]
        end
        for l=0:N-1-j
            M[j+1,l+1] += b[l+j+1]
        end
    end

    return 2*(M\C)
end

function Remove_theta(C::Array{Float64,1},d::Float64, min::Float64, max::Float64)::Array{Float64,1}
    d_scaled = (2*d - max - min)/(max-min)
    return Remove_theta(C, d_scaled)
end

"""
    Remove_theta_rev(C, d, min, max)

Given the coefficients C in [min, max] of f(x)*[1-θ(x-d)]
it tries to extract the coefficients of f in [min, d].
"""
function Remove_theta_rev(C::Array{Float64,1},d::Float64)::Array{Float64,1}
    N=length(C)
    M=zeros(N,N)
    b= - Fun2Coeffs(-1., 1., N, N, x->float(x>d))
    b[1] = b[1]+1
    #j=0 is special
    M[1, 1] += b[1]
    for l=0:N-1
        M[1,l+1] += b[l+1]
    end

    for j=1:N-1
        for l=0:j
            M[j+1,l+1] += b[j-l+1]
        end
        for l=j:N-1
            M[j+1,l+1] += b[l-j+1]
        end
        for l=0:N-1-j
            M[j+1,l+1] += b[l+j+1]
        end
    end

    return 2*(M\C)
end

function Remove_theta_rev(C::Array{Float64,1},d::Float64, min::Float64, max::Float64)::Array{Float64,1}
    d_scaled = (2*d - max - min)/(max-min)
    return Remove_theta_rev(C, d_scaled)
end

"""
    ClenshawCurtisWeights(N)

Compute the Clenshaw-Curtis weights of the interval [-1,1].
Optional arguments: an interval [a,b] for the scaled weights,
                    the extrema a, b of the interval.
"""
function ClenshawCurtisWeights(N::Int64)::Array{Float64,1}
    w = zeros(N)
    tmp=0.
    for j=0:N-1
        tmp = (2*j+1)/2.0/N
        w[j+1] = 2/N + 4/N*sum( [ cospi(tmp*k) / (1-k^2) for k=2:2:N ] )
    end
    return w
end

function ClenshawCurtisWeights(N::Int64, Inter::Array{Float64,1})::Array{Float64,1}
    return (Inter[end] - Inter[1])/2 * ClenshawCurtisWeights(N)
end

function ClenshawCurtisWeights(N::Int64, a::Float64, b::Float64)::Array{Float64,1}
    return (b-a)/2 * ClenshawCurtisWeights(N)
end

"""
    DiffCheb(c)

Compute the coefficients of the derivative of a chebyshev series with coefficients c in [-1,1]
c.

DiffCheb(c,a,b) for the interval [a,b]
"""
function DiffCheb(c::Array{Float64,1})::Array{Float64,1}
    N=length(c)
    c_prime = zeros(N)
    j_max_2 = mod(N,2)==0 ? N÷2-1 : N÷2
    for j=0:(N ÷ 2-1)
        c_prime[2*j + 1] = sum( [ (2k-1)*c[2k] for k = j+1:N÷2] ) #c[2k-1+1] actually
        c_prime[2*j + 2] = sum( [ (2k)*c[2k+1] for k = j+1:j_max_2] ) #c[2k + 1]
    end
    c_prime[1] *= 0.5

    return 2*c_prime[1:end-1]
end

function DiffCheb(c::Array{Float64,1}, min::Float64, max::Float64)::Array{Float64,1}
    return DiffCheb(c) * 2/(max-min)
end

"""
    DiffChebVals(fj,a,b)

Computes the derivative of f at the Chebyshev points of [a,b]
"""

function DiffChebVals(fj::Vector{Float64})
    N=length(fj)
    dfj = zeros(N)
    for j=1:N
        fj_j = fj[j]
        (sinj,cosj) = sincospi((2N-2j+1)/2N)
        for i=1:N
            (sini,cosi) = sincospi((2N-2i+1)/2N)
            fact = i==j ? 0.5*cosj/sinj^2 : (-1)^(i+j+1) * sinj/sini/ (cosj-cosi)
            dfj[i] += fact*fj_j
        end
    end

    return dfj
    ##### To generate the Matrix
        # x = chebpts(N, 0.,1.)
        # M=zeros(N,N)
        # for i=1:N
        #     for j=1:N
        #         M[i,j] = i==j ?  x[j]/2/(1-x[j]^2) : (-1)^(i+j+1) * sqrt(1-x[j]^2)/sqrt(1-x[i]^2) / (x[j]-x[i])
        #     end
        # end
end

function DiffChebVals(c::Array{Float64,1}, min::Float64, max::Float64)::Array{Float64,1}
    return DiffChebVals(c) * 2/(max-min)
end

"""
    LinearChebInterp(fj,x, min, max)

Linearly interpolates a function at x in the interval [min, max] given its values at the chebpts.
"""
function LinearChebInterp(fj::Array{Float64,1},x0::Float64,min::Float64, max::Float64)::Float64
    Nc = length(fj)
    x = 2*(copy(x0) .- min)/(max-min) - 1
    idx = floor(Int,Nc + 0.5 .- Nc/pi * acos.(x))
    if idx == 0
        y = fj[1] + (fj[2] - fj[1])/(cospi((2Nc - 3)/2Nc) - cospi((2Nc -1)/2Nc)) * (x - cospi((2Nc - 1)/2Nc))
    elseif idx == Nc
        y = fj[end] + (fj[end] - fj[end-1])/(cospi(1/2Nc) - cospi(3/2Nc)) * (x - cospi(1/2Nc))
    else
        y = fj[idx] + (fj[idx+1] - fj[idx])/(cospi((2Nc - 2idx-1)/2Nc) - cospi((2Nc - 2idx + 1)/2Nc)) * (x - cospi((2Nc - 2idx +1)/2Nc))
    end

    return y
end

function LinearChebInterp(fj::Array{Float64,1},x0::Array{Float64,1},min::Float64, max::Float64)::Array{Float64,1}
    Nc = length(fj)
    x = 2*(copy(x0) .- min)/(max-min) .- 1
    Nx = length(x)
    y=zeros(Nx)
    i = 0
    idx = floor(Int,Nc + 0.5 .- Nc/pi * acos.(x[1]))
    @show idx
    while idx == 0 && i < Nx
        i += 1
        idx = floor(Int,Nc + 0.5 .- Nc/pi * acos.(x[i]))
        if idx == 0
            y[i] = fj[1] + (fj[2] - fj[1])/(cospi((2Nc - 3)/2Nc) - cospi((2Nc -1)/2Nc)) * (x[i] - cospi((2Nc - 1)/2Nc))
        else
            i -=1
        end
    @show idx
        
    end
    while idx < Nc-1 && i < Nx
        i += 1
        idx = floor(Int,Nc + 0.5 .- Nc/pi * acos.(x[i]))
        if idx< Nc-1 
            y[i] = fj[idx] + (fj[idx+1] - fj[idx])/(cospi((2Nc - 2idx-1)/2Nc) - cospi((2Nc - 2idx + 1)/2Nc)) * (x[i] - cospi((2Nc - 2idx +1)/2Nc))
        end
    @show idx
    
    end
    y[i:end] = fj[end] .+ (fj[end] - fj[end-1])/(cospi(1/2Nc) - cospi(3/2Nc)) * (x[i:end] .- cospi(1/2Nc))
        
    return y
end

"""
    LinearArgChebInterp(fj,x, min, max)

Linearly interpolate in θ a function of x=cos(θ) in the interval [min, max] given its values at the chebpts.
"""
function LinearArgChebInterp(fj::Array{Float64,1},x0::Float64,min::Float64, max::Float64)::Float64
    Nc = length(fj)
    x = 2Nc - acos.(2*(copy(x0) .- min)/(max-min) - 1)/pi*2Nc + 1
    idx = Int(x ÷ 2)
    
    if idx == 0
        y = fj[1] + (fj[2] - fj[1])/2*(x - 2)
    elseif idx == Nc
        y = fj[end] + (fj[end] - fj[end-1])/2 * (x - 2*Nc )
    else
        y = fj[idx] + (fj[idx+1] - fj[idx])/2 * (x - 2*idx)
    end

    return y
end

function LinearArgChebInterp(fj::Array{Float64,1},x0::Array{Float64,1},min::Float64, max::Float64)::Array{Float64,1}
    Nc = length(fj)
    x = 2Nc .+ 1 .- acos.(2*(copy(x0) .- min)/(max-min) .- 1)/pi*2Nc
    Nx = length(x)
    y=zeros(Nx)
    i = 1
    while i < Nx && x[i]<2
        y[i] = fj[1] + (fj[2] - fj[1])/2*(x[i] - 2)
        i += 1
    end
    idx = Int(x[i] ÷ 2)
    while idx < Nc-1 && i < Nx
        y[i] = fj[idx] + (fj[idx+1] - fj[idx])/2 * (x[i] - 2*idx)
        i += 1
        idx += Int( (x[i] - 2idx) ÷ 2 )
    end
    if i==Nx && idx < Nc-1
        y[i] = fj[idx] + (fj[idx+1] - fj[idx])/2 * (x[i] - 2*idx)
    else
        y[i:end] = fj[end-1] .+ (fj[end] - fj[end-1])/2 * (x[i:end] .- 2Nc .+ 2 )
    end

    return y
end

"""
    InterpOnCheb(fj,x, min, max, N)

Linearly interpolate the function (x,f) in [min, max] at the corresponding Chebyshev grid of order N.
"""
function InterpOnChebArg(fj::Vector{Float64},x0::Vector{Float64}, min::Float64, max::Float64, N::Int64)::Vector{Float64}
    Nx = length(fj)
    i = 1
    x = 2N .+ 1 .- acos.(2*(copy(x0) .- min)/(max-min) .- 1)/pi*2N
    y = zeros(N)
    j = 2
    while j < x[i] && j <= 2N
        y[Int(j/2)] = fj[1] + (fj[2] - fj[1])/(x[2]-x[1])*(x[1] - j)
        j += 2
    end
    i += 1
    while j <= 2N && i < Nx
        while j < x[i] && j <= 2N
            y[Int(j/2)] = fj[i-1] + (fj[i] - fj[i-1])/(x[i]-x[i-1]) *(j-x[i-1])
            j += 2
        end
        i += 1
    end
    y[Int(j/2):end] = fj[end-1] .+ (fj[end] - fj[end-1])/(x[end] - x[end-1])*(collect(j:2:2N) .-x[end-1])

    return y
end

function InterpOnCheb(fj::Vector{Float64},x0::Vector{Float64}, min::Float64, max::Float64, N::Int64)::Vector{Float64}
    Nx = length(fj)
    i = 1
    j=1
    x = 2*(x0 .- min)/(max-min) .- 1
    y = zeros(N)
    xj = cospi((2N-1)/2N)
    while xj < x[i] && j <= N
        y[j] = fj[1] + (fj[2] - fj[1])/(x[2]-x[1])*(xj - x[1])
        j += 1
        xj = cospi((2N-2j+1)/2N)
    end
    i += 1
    while j <= N && i < Nx
        xj = cospi((2N-2j+1)/2N)
        while xj < x[i] && j <= N
            y[j] = fj[i-1] + (fj[i] - fj[i-1])/(x[i]-x[i-1]) *(xj-x[i-1])
            j += 1
            xj = cospi((2N-2j+1)/2N)
        end
        i += 1
    end
    xj = cospi.((2N .- 2collect(j:N) .+ 1)/2N)
    y[j:end] = fj[end-1] .+ (fj[end] - fj[end-1])/(x[end] - x[end-1])*(xj .-x[end-1])

    return y
end

"""
    DiffChebValsDiff(fj,a,b)

Compute the finite difference derivative using linear interpolation.

"""
function  DiffChebValsDiff_interpArg(fj,a,b)
    Nc = length(fj)
    x_new = collect( LinRange(a+(b-a)*(1-cospi(1/2Nc))/2,b-(b-a)*(1-cospi(1/2Nc))/2,Nc) )
    dy = zeros(Nc)
    let y = LinearArgChebInterp(fj, x_new,a,b)
        dy[1] = (y[2]-y[1])/(x_new[2] - x_new[1])
        dy[2:end-1] = (y[3:end]-y[1:end-2])./(x_new[3:end] - x_new[1:end-2])
        dy[end] = (y[end]-y[end-1])/(x_new[end] - x_new[end-1])
    end
    dy = InterpOnChebArg(dy,x_new,a,b,Nc)

    return dy
end

function  DiffChebValsDiff(fj,a,b)
    Nc = length(fj)
    x_new = collect( LinRange(a+(b-a)*(1-cospi(1/2Nc))/2,b-(b-a)*(1-cospi(1/2Nc))/2,Nc) )
    dy = zeros(Nc)
    let y = LinearArgChebInterp(fj, x_new,a,b)
        dy[1] = (y[2]-y[1])/(x_new[2] - x_new[1])
        dy[2:end-1] = (y[3:end]-y[1:end-2])./(x_new[3:end] - x_new[1:end-2])
        dy[end] = (y[end]-y[end-1])/(x_new[end] - x_new[end-1])
        
        ### One order above
        # dy[1] = (y[2]-y[1])/(x_new[2] - x_new[1])
        # dy[2] = (y[3]-y[1])./(x_new[3] - x_new[1])
        # dy[3:end-2] = (-y[5:end]+8y[4:end-1]-8y[2:end-3] + y[1:end-4])/(x_new[2]-x_new[1])/12
        # dy[end-1] = (y[end]-y[end-2])./(x_new[end] - x_new[end-2])
        # dy[end] = (y[end]-y[end-1])/(x_new[end] - x_new[end-1])
    end
    dy2 = InterpOnCheb(dy,x_new,a,b,Nc)

    return dy2
end

"""
    DiffChebValsDiffChebVals(fj,Nc)

Compute the derivative at the chepts of length(fj) of the N-truncated series of f given the values of fj.

"""
function  DiffChebValsDiffChebVals(fj::Vector{Float64},N::Int64)
    fl = ChebVals2Val(fj, chebpts(N))
    dfl = DiffChebVals(fl)
    return ChebVals2Val(dfl, chebpts(length(fj)))
end

function  DiffChebValsDiffChebVals(fj::Vector{Float64},N::Int64, min::Float64, max::Float64)
    fl = ChebVals2Val(fj, chebpts(N, min, max), min, max)
    dfl = DiffChebVals(fl,min,max)
    return ChebVals2Val(dfl, chebpts(length(fj),min,max), min, max )
end