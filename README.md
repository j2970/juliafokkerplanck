# juliafokkerplanck

*Warning: this is not an official release, there is no proper documentation and parts of the code may be unsufficiently commented.
Feel free to contact the author for help with the code or any suggestions.*

This code solves the Orbit averaged Fokker Planck equation that describes the evolution of glactic nuclei dominated by a central massive black hole and includes the loss-cone treatment for gravitational captures.
The stellar cluster is modeled with two components, one giving tidal disruption events (TDEs) and one captured via direct plunges or on Extreme Mass Ratio inspirals (EMRIs).

The code is written in Julia and is multi-threaded. The idea is to use

- Julia multithreading where possible,
- OpenBlas through the corresponding wrapper to compute sparse matrices multiplications.

## Getting started
The simpler way to try this code is to clone this directory and launch the example simulation with the command
```
include("main.jl")
```
If launched in this way, Julia will complain about missing packages and - from version 1.7.0 - interactively ask to install them.

An alternative way is to launch the script with
```
julia main.jl
```
and the output will show missing packages.

To launch the code with multiple threads, it is sufficient to follow the usual Julia way: on a Linux machine, either define the global variable "JULIA_NUM_THREADS" or launch with the command `julia -t NT` where `NT` is the desired number of threads.

The default simulation is a system with a central black hole of 4*10^6 solar masses surrounded by a distribution of stars and black holes.
The latter follow a Dehnen profile with inner slope of 1.5 and a scale radius set as ra = 4*GM/sigma^2, where sigma comes from the M-sigma relation
(Gutielkin+ 2009). The total number of stars is 8*10^7 and the total number of compact objects is 8*10^4.

## Structure of the code
The code is launched via the `main.jl` file, which calls `IC_FP` for generating initial conditions and `evolution!` for time evolution.

These functions are contained in the file `FokkerPlanck.jl`, which contains the actual code.

In the file `FokkerPlanckAnalysis.jl` there are some useful functions that are useful to analyse a set of snapshot, including a function to translate the DF into a mass density.

The file `MyCheb.jl` is a library to work with Chebyshev polynomials and their roots that is only partly used in the code.

## Authors
- Luca Broggi, Università degli Studi di Milano-Bicocca. (l.broggi1_AT_campus.unimib.it)
    
    Supervisors: Elisa Bortolas, Matteo Bonetti, **Alberto Sesana**, **Massimo Dotti**


## Publications
The following publications have been realized using this code

- *Extreme mass ratio inspirals and tidal disruption events in nuclear clusters. I. Time dependent rates.*, L. Broggi et al (2022)  [[arXiv](https://arxiv.org/abs/2205.06277), [DOI](https://doi.org/10.1093/mnras/stac1453)]

Please contact the author if you use this code and would like to have your publication listed here.

