include("FokkerPlanck.jl")

params = Dict(  "delta"     => 1e-3,   # number of compact objects / number of extended objects
				"M"         => 4e6,
                "ms"        => 1.,
                "mbh"       => 10.,
                "sigma"     => 1,
                "γ"         => 1.5,
                "M_u"       => 1.98892e30,  # 1 solar mass
                "r_u"       => 3.0856775814672e16, # 1 pc
                "fin_cond"  => "time", # with Pan&Yang the condition is dNdt_bh|Egw = 0
                "NgridR"    => 200,
                "Ngrids"    => 200,
                "N_coeffs"  => 100,    # number of points for Gauss quadratures
                "epsilon"   => 0.03,   # maximum relative change at each timestep (set in this way)
                "N_steps"   => 15000,  # maximum number of steps
                "N_substeps"=> 5,      # number of substeps, i.e. without recomputing the diffusion coefficients
                "N_save"    => 10,     # number of substeps between snapshots
                "stars_lc"  => "tde",  # tde gives a different loss_cone for stars, loss_cone the same 8 GM/c^2
                "algorithm" => 1,      # [1] = flux conservative
                "scheme"    => 2,      # [1] = explicit (unstable, under development), 2 = implicit
            )

params["Ns"]    = 20*params["M"]                                                        # total number of stars
params["v_u"]   = (70*10^3 * (params["M"]/1.53e6)^(1/4.24))                             # velocity unit: sigma from M-sigma
params["t_fin"] = 0.75*2.1*10^9 * (3600*24*365) / (params["r_u"]/params["v_u"])               # final time in internal units
params["filename"] = "snapshots_withdiffrate_$(params["M"])/snap_grid_$(params["NgridR"])x$(params["Ngrids"])"
params["final_output"] = "summary_grid_withdiffrate_$(params["M"])_$(params["NgridR"])x$(params["Ngrids"]).jld2"

#Initialization
println("Initialization.")
f0, aux     = IC_FP(params)
f=copy(f0)


println("Starting the evolution. Final output in:")
@show params["filename"] params["final_output"]

t, f_e, gamma, gamma_egw, n_inside, n_tot, gamma_s, gamma_egw_s, n_inside_s, n_tot_s       = evolution(f, params, aux, 0.0)

println("Evolution ended. Final output in:")

@show params["filename"] params["final_output"]
