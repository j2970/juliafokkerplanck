using JLD2, FileIO
include("FokkerPlanckAnalysis.jl")
include("FokkerPlanck.jl")

sum_file       = load("summary_grid_100x100.jld2")
f0, aux        = IC_FP(sum_file["params"])
rho_stars, r   = f2rho(sum_file["f_e"][:,:,1], sum_file["params"], aux)
rho_bh, r   = f2rho(sum_file["f_e"][:,:,2], sum_file["params"], aux)

jldsave("density.jld2"; r, rho_stars, rho_bh)
